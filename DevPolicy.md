# 開発環境  
Visual Studio Code

# 言語
Python 3.6(64bit)

# 使用ライブラリ
chainer, numpy

# 開発flow  
1. モジュールIssueの作成
1. 空モジュールの作成  
1. クラスIssueの作成
1. 空クラスの作成
1. 関数Issueの作成
1. 空関数の作成
1. GitFlowに基づき、featureブランチ内にモジュールブランチを作成
1. ファイルの追加、変更、修正、削除
1. コミット
1. モジュールブランチをpush
1. モジュールブランチが完成したら、developブランチへマージ
1. 動作確認
1. developブランチをpush

# 空モジュール・クラス・関数について
空モジュール・クラス・関数とは、Issueに基づいて、仕様コメントを追加したモジュール・クラス・関数のこととする。

# コミットメッセージについて
- [モジュール名].[クラス名].[関数名]の[追加、変更、修正、削除]  
  詳細の説明 #X
- [モジュール名].[クラス名].[関数名]の[追加、変更、修正、削除]  
  詳細の説明 #X
...
